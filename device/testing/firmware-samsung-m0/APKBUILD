pkgname=firmware-samsung-m0
pkgver=2
pkgrel=0
pkgdesc="Samsung Galaxy S III Firmware"
_commit="fdd8c44568a148c22fbf008de822ff4e27992da1"
url="https://github.com/TheMuppets/proprietary_vendor_samsung"
_armbian_commit="5ff122ef309a1ddabb4bf7f37c8347c43b314a61"
_armbian_url="https://github.com/armbian/firmware"
arch="armv7"
license="proprietary"
subpackages="
	$pkgname-mainline
	$pkgname-downstream
	"
source="
	$url/raw/$_commit/smdk4412-common/proprietary/bin/bcm4334.hcd
	bcm4334_nvram_net.txt::$url/raw/$_commit/smdk4412-common/proprietary/etc/wifi/nvram_net.txt
	$url/raw/$_commit/smdk4412-common/proprietary/etc/wifi/bcmdhd_sta.bin_b2
	$_armbian_url/raw/$_armbian_commit/brcm/brcmfmac4334-sdio.bin
	$_armbian_url/raw/$_armbian_commit/brcm/brcmfmac4334-sdio.rockchip%2Crk3318-box.txt
"
options="!check !strip !archcheck !spdx !tracedeps pmb:cross-native"

package() {
	mkdir -p "$pkgdir"/lib/firmware/postmarketos

	install -m644 "$srcdir"/bcm4334.hcd \
		"$pkgdir"/lib/firmware/postmarketos/
}

mainline() {
	depends="$pkgname"
	mkdir -p "$subpkgdir"/lib/firmware/brcm \
		"$subpkgdir"/lib/firmware/postmarketos

	install -m644 "$srcdir"/brcmfmac4334-sdio.bin \
		"$subpkgdir"/lib/firmware/postmarketos/brcmfmac4334-sdio.bin
	ln -s /lib/firmware/postmarketos/brcmfmac4334-sdio.bin \
		"$subpkgdir"/lib/firmware/brcm/brcmfmac4334-sdio.samsung,i9300.bin
	ln -s /lib/firmware/postmarketos/brcmfmac4334-sdio.bin \
		"$subpkgdir"/lib/firmware/brcm/brcmfmac4334-sdio.samsung,i9305.bin

	install -m644 "$srcdir"/brcmfmac4334-sdio.rockchip%2Crk3318-box.txt \
		"$subpkgdir"/lib/firmware/postmarketos/brcmfmac4334-sdio.txt
	ln -s /lib/firmware/postmarketos/brcmfmac4334-sdio.txt \
		"$subpkgdir"/lib/firmware/brcm/brcmfmac4334-sdio.samsung,i9300.txt
	ln -s /lib/firmware/postmarketos/brcmfmac4334-sdio.txt \
		"$subpkgdir"/lib/firmware/brcm/brcmfmac4334-sdio.samsung,i9305.txt

	ln -s /lib/firmware/postmarketos/bcm4334.hcd \
		"$subpkgdir"/lib/firmware/brcm/BCM4334B0.hcd
}

downstream() {
	depends="$pkgname"
	mkdir -p "$subpkgdir"/lib/firmware/postmarketos/

	install -m644 "$srcdir"/bcm4334_nvram_net.txt \
		"$pkgdir"/lib/firmware/postmarketos/nvram_net.txt
	install -m644 "$srcdir"/bcmdhd_sta.bin_b2 \
		"$subpkgdir"/lib/firmware/postmarketos/
}

sha512sums="
cbed1dc60829161c7ac9649dcae67fc45204c1a25e8de320ad3bc4e9d5cae573fb8a4745f91355fa96d2d3d4124a9e707341fc5bd1b22705cb2efb16831e029f  bcm4334.hcd
f6a936c526ace433b0268f2e35205776e17496fb14852deaf21f07e68ff16ac99963ab94c93094a58019bf514f11a415c1a0fc4a9e4cd523047ffefd9c6c55d8  bcm4334_nvram_net.txt
4f29f977acfa48437bb427ad49a03021bc98af9623575ecfb3e994a6ebcaa99899e843550b4080a0e16bca2076a0a9bd1788c1ff543e36687c2b3196b56e6099  bcmdhd_sta.bin_b2
528b8087ce2d985c397b1072070bd743114b7176340cd7612ca5d293e2059b1c483522c3adc4f58c4daf738848e95d3541195608f47acc5236032b6a2bfee441  brcmfmac4334-sdio.bin
d5c83e1dcd67630e186ab484d150ca5c0af12810de62a46ec6e525b50dabf64a75f1231ef8d7c1d102fb76389210fdb952975460179ca767acd0ffdd869907d7  brcmfmac4334-sdio.rockchip%2Crk3318-box.txt
"
